const fs = require('fs')
const Stripe = require('stripe');
const STRIPE_TEST_SECRET_KEY = 'rk_test_51EDa6GIrkMUcWfFwXon9vsJYpTqX2eTqbINAUf4fZC7ivToWv59cAPoHdYhmszwL9ZKWJtUUbaCcHtpjl6rWlXLP00C1dcAoUR'
const stripe = Stripe(STRIPE_TEST_SECRET_KEY);

const handler = async (country) => {

  try{
    let finalCustomers = [];

    /*********************************** FUNCTIONS ******************************/

    /* Filter customers by country */
    function filterCustomers(customers, cntry) {
      let filteredList = [];
      for (var index in customers){
        if (customers[index].country===cntry){
          filteredList.push(customers[index])
        }
      }
      return filteredList;
    }

    /* Get country code for specified country */
    function getCountryCode(cntry) {
      var countryCodesJson = JSON.parse(fs.readFileSync('./countries-ISO3166.json'));
      return Object.keys(countryCodesJson).find(code => countryCodesJson[code] === cntry);
    }
    
    /* Transform customers for future Stripe customer creation */
    function transformForStripe(customers) {
      var countryCode = getCountryCode(country);
      let transformedCustomers = [];
      let c = {};
      for (var index in customers){
        c = {
          name: customers[index].first_name+" "+customers[index].last_name,
          address: {
            line1: customers[index].address_line_1,
            country: countryCode
          },
          
          email: customers[index].email
        }
        transformedCustomers.push(c);
      }
      return transformedCustomers;
    }

    /* for each customer create a Stripe customer & push it into finalCustomers with email, country and id as properties. */
    async function createStripeCustomers(customers) {
      for(var index in customers){
        let stripeCustomer = await stripe.customers.create(customers[index]);
        finalCustomers.push({
          email: stripeCustomer.email,
          country: stripeCustomer.address.country,
          customerId: stripeCustomer.id
        });
      }
    }

    /* write finalCustomers array into final-customers.json using fs */
    function wrtiteToFinalCustomers(){
      fs.writeFile('./final-customers.json', JSON.stringify(finalCustomers), (err) => {
        if (err) throw err;
        console.log('-Data written to final-customers.json');
      });
    }

    /****************************************** END OF FUNCTIONS ********************************************/

    var customersJson = JSON.parse(fs.readFileSync('./customers.json'));
    let filteredCustomers = filterCustomers(customersJson, country);
    let transformedCustomers = transformForStripe(filteredCustomers);
    await createStripeCustomers(transformedCustomers);
    wrtiteToFinalCustomers();
    
    console.log("-SUCCESS");
    console.log("-finalCustomers: ");
    console.log(finalCustomers);

  }catch(e){
    console.log("-ERROR");
    throw e;
  }
} 

(handler("Spain"));
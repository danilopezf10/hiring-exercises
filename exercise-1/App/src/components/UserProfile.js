import React from 'react';
import UserInfoCard from './UserInfoCard'
import UserPostsCard from './UserPostsCard'

const UserProfile = (props) => {
    return(
        <div class="max-w-4xl flex items-center h-auto lg:h-screen flex-wrap mx-auto my-32 lg:my-0">
            <UserInfoCard id={props.id}/>
            <UserPostsCard id={props.id} limit={props.limit}/>
        </div>
    )
}

export default UserProfile

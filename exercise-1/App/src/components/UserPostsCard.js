import React from 'react';
import { useQuery, gql } from '@apollo/client';
import Loading from './Loading'
import ErrorComponent from './ErrorComponent'
import UserPostsLayout from './UserPostsLayout'

const UserPostsCard = (props) => {
    /* GQL query to get the user's x first posts, where x < limit. Limit = 10 if not specified otherwise*/
    const GET_USER_POSTS = gql`
        {
            user(id: ${props.id}){
                name
                posts(options:{paginate:{page:1,limit:${props.limit || 10}}}) {
                    data {
                        id
                        title
                    }
                }
            }
        }
        `;
    const { loading, error, data } = useQuery(GET_USER_POSTS);
    if (loading) return <Loading />;
    if (error) return <ErrorComponent error={error} />;

    return(
       <UserPostsLayout>  
            <p class="pt-4 text-base font-bold flex items-center justify-center lg:justify-start mt-4">{data.user.name}'s Posts:</p>
            {data.user.posts.data.map(post =>
                <p key={post.id} class="pt-2 text-gray-600 text-xs lg:text-sm flex items-center justify-center lg:justify-start">Title: {post.title}</p>
            )} 
        </UserPostsLayout>   
    )
}

export default UserPostsCard

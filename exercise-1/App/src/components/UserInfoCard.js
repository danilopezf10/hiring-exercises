import React from 'react';
import { useQuery, gql } from '@apollo/client';
import Loading from './Loading'
import ErrorComponent from './ErrorComponent'
import UserInfoLayout from './UserInfoLayout'

const UserInfoCard = (props) => {
    /* GQL query to get the user's info */
    const GET_USER_INFO = gql`
        {
            user(id: ${props.id}) {
                name
                address {street,suite,city}
                email
                phone
                company{name}
            }
        }
        `;
    const { loading, error, data } = useQuery(GET_USER_INFO);
    if (loading) return <Loading />;
    if (error) return <ErrorComponent error={error} />;

    return(
        <UserInfoLayout> 
            <h1 class="text-3xl font-bold pt-8 lg:pt-0">{data.user.name}</h1>
            <div class="mx-auto lg:mx-0 w-4/5 pt-3 border-b-2 border-teal-500 opacity-25"></div>
            <p class="pt-4 text-base font-bold flex items-center justify-center lg:justify-start">Address:</p>
            <p class="pt-2 text-gray-600 text-xs lg:text-sm flex items-center justify-center lg:justify-start"> {data.user.address.street}, {data.user.address.suite}, {data.user.address.city}</p>
            <p class="pt-4 text-base font-bold flex items-center justify-center lg:justify-start mt-4">Email:</p>
            <p class="pt-2 text-gray-600 text-xs lg:text-sm flex items-center justify-center lg:justify-start">{data.user.email}</p>
            <p class="pt-4 text-base font-bold flex items-center justify-center lg:justify-start mt-4">Phone:</p>
            <p class="pt-2 text-gray-600 text-xs lg:text-sm flex items-center justify-center lg:justify-start">{data.user.phone}</p>
            <p class="pt-4 text-base font-bold flex items-center justify-center lg:justify-start mt-4">Company:</p>
            <p class="pt-2 text-gray-600 text-xs lg:text-sm flex items-center justify-center lg:justify-start">{data.user.company.name}</p>
        </UserInfoLayout>
    )
}

export default UserInfoCard

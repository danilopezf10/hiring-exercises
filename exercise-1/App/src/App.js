import React from 'react';
import UserProfile from './components/UserProfile'
import { ApolloProvider } from '@apollo/client';
import { ApolloClient, InMemoryCache } from '@apollo/client';

const client = new ApolloClient({
  uri: 'https://graphqlzero.almansi.me/api',
  cache: new InMemoryCache()
});

const userId = 1, limit = 10;

function App() {
  document.body.style = "background-image:url('https://source.unsplash.com/1L71sPT5XKc')";
  return (
    <div className="App font-sans antialiased text-gray-900 leading-normal tracking-wider bg-cover p12" >
      <ApolloProvider client={client}>
        <UserProfile id={userId} limit={limit}/>
      </ApolloProvider>
    </div>
  );
}

export default App;
